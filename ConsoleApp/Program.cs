﻿using System;
using System.Text;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;

            Console.WriteLine("Welcome");

            Console.Write("> Enter email to\n> ");
            string email = Console.ReadLine();

            Console.Write("> Enter subject\n> ");
            string subject = Console.ReadLine();

            Console.Write("> Enter text\n> ");
            string text = Console.ReadLine();

            Sender sender = new Sender();
            sender.Send(new MailModel
            {
                Email = email,
                Subject = subject,
                Text = text
            }).Wait();

            Console.WriteLine("Mail was send successfully!");
        }
    }
}
