﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class Sender
    {
        private string _from = "example@gmail.com";
        private string _host = "smtp.gmail.com";
        private string _login = "example@gmail.com";
        private string _password = "password";
        private int _port = 465;

        public async Task Send(MailModel model)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Lab", _from));
            message.To.Add(new MailboxAddress("User", model.Email));
            message.Subject = model.Subject;
            message.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = model.Text
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_host, _port, true);
                await client.AuthenticateAsync(_login, _password);
                await client.SendAsync(message);

                await client.DisconnectAsync(true);
            }

        }
    }

    public class MailModel
    {
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Text { get; set; }
    }
}
